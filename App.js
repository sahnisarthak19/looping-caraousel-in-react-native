import * as React from 'react';
import {
  Text,
  View,
  Image,
  SafeAreaView,
  Alert,
  TouchableOpacity,
} from 'react-native';

import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Dimensions } from 'react-native';

export default class App extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      entries: [
        {
          title: "Item 1",
          text: "https://www.setaswall.com/wp-content/uploads/2017/10/Vermillion-Lake-Stars-Qhd-Wallpaper-1080x1920.jpg",
        },
        {
          title: "Item 2",
          text: "https://www.teahub.io/photos/full/7-70756_wallpapers-full-hd-1080p-for-mobile-xiaomi-redmi.jpg",
        },
        {
          title: "Item 3",
          text: "https://wallpaperaccess.com/full/57166.jpg",
        },
        {
          title: "Item 4",
          text: "https://wallpapercave.com/wp/wp2597278.jpg",
        },
        {
          title: "Item 5",
          text: "https://wallpapercave.com/wp/wp2875581.jpg",
        },
      ]
    }
  }

  _renderItem({ item, index }) {
    return (
      <View style={{
        backgroundColor: '',
        borderRadius: 10,
        height: 246,
        padding: 0,
        marginLeft: 0,
        marginRight: 0,
      }}>
        {/* <Text style={{fontSize: 30}}>{item.title}</Text> */}
        <TouchableOpacity onPress={() => {alert("Image selected")}}>
          <Image
            source={{ uri: item.text }}
            style={{ width: 180, height: 246, borderRadius: 10}}
          ></Image>
        </TouchableOpacity>

      </View>

    )
  }

  get pagination () {
    const { activeIndex, entries } = this.state;
    return (
        <Pagination
          dotsLength={entries.length}
          activeDotIndex={activeIndex}
          dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 8,
              backgroundColor: "orange"
          }}
          inactiveDotStyle={{
              // Define styles for inactive dots here
          }}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
    );
}

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '', paddingTop: 200 }}>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
          <Carousel
            layout={"default"}
            ref={ref => this.carousel = ref}
            data={this.state.entries}
            sliderWidth={Dimensions.get('window').width}
            itemWidth={180}
            renderItem={this._renderItem}
            loop={true}
            onSnapToItem={index => this.setState({ activeIndex: index })} />
            
        </View>
        { this.pagination }
      </SafeAreaView>
    );
  }
}